#!/usr/bin/python

"""
This module defines a linear programming problem solver using an implementation of the simplex method.

See documentation for the `LinearProgram` class for more information on
implementation details and usage.

Currently, the solver only deals with problems in standard form. ie:
  1. The problem must be a maximization problem
  2. Each constraint must be of the form Ax <= b
  3. Each variable x_i >= 0

In the future, it will be able to do minimization problems with constraints
of the form Ax >= b by following the steps described in:
http://college.cengage.com/mathematics/larson/elementary_linear/4e/shared/downloads/c09s4.pdf

Resources:
  - https://people.richland.edu/james/ictcm/2006/simplex.html
  - https://www.youtube.com/watch?v=gRgsT9BB5-8
"""

import numpy as np

class LinearProgram(object):
    """
    Constructs a Linear Programming problem in standard form to maximize.

    Usage:
      >> lp = LinearProgram( [1, 2] )    # Maximize:   x + 2y = P
      >> lp.addConstraint( [1, 1], 100 ) # Constraint: x + y <= 100
      >> lp.addConstraint( [2, 0], 10 )  # Constraint: 2x <= 10
      >> lp.solve()
      

    Tableau:
      [ -o1 -o2 -o3  P ... | 0  ]
      [ ------------------ | -- ]
      [ c1  c2   c3  0 ... | b1 ]
      [ d1  d2   d3  0 ... | b2 ]
      [         .          |    ]
      [         .          |    ]

      ox: coefficients of objective function
      P : the optimization function value (what we're trying to maximize)

      cx: coefficients of constraint 'c'
      dx: coefficients of constraint 'd'

      bx: the values for each of the constraints
    """

    def __init__( self, obj ):
        """
        Creates a new Linear Programming problem.
        
        Parameters:
          - obj: Objective function to maximize
        """

        # Function of form P = c_1 x_1 + c_2 x_2 ...
        #   Turn into form 0 = P + -c_1 x_1 + -c_2 x_2... to add to tableau (positive P)
        #   Setting objective to [ P -c_1 -c_2 -c_3 ... ]
        self.Tableau = np.mat( [ -1 * val for val in obj ] + [1] )
        self.Values  = np.mat([0])

        self.NumVars = len(obj)

    def addConstraint( self, row, val ):
        """
        Adds a constraint of the form c_1 x_1 + c_2 x_2 ... <= Y.

        Parameters:
          - row: A list of coefficients c_1 ... c_i
          - val: The constraint value. Y in the above equation.
        """

        nrows, ncols = self.Tableau.shape

        # Add zeroes to the row to match the size of the array
        row_array = np.array( [row] )
        row_array.resize(1, ncols)
        self.Tableau = np.vstack( [ self.Tableau, row_array ] ) # Add row

        # Add slack variable to turn into equality
        #   Result will be a new column of the form [0 0 0 ... 1]'
        nrows, ncols = self.Tableau.shape
        slack_vars = np.zeros( nrows )
        slack_vars[-1] = 1

        self.Tableau = np.hstack( [self.Tableau, np.transpose([slack_vars])] ) # Add column
        self.Values = np.vstack( [self.Values, [val]] ) # Add row to values

    def getPivot( self ):
        """
        Get the row and column to pivot on this iteration.

          pivot column is the most negative number still in the objective function.
          pivot row    is the row w/ the lowest ratio between the coefficient and the value
        """

        pivot_col = np.argmin(self.Tableau[0,:]) # Min of obj row
        ratios = self.Values[1:] / self.Tableau[:,pivot_col][1:]
        pivot_row = np.argmin( ratios ) + 1 # Add the one back (from missing Objective row)

        if all( [x < 0 for x in ratios] ):
            return None, None

        return pivot_row, pivot_col

    def isSolved( self ):
        """
        Check if the problem is optimally solved.

        Solved if all of the objective functions are non-negative.
        """
        return not np.any( self.Tableau[0,:] < 0 )

    def pivot( self, row, col ):
        """
        Pivot along the row and column in the tableau.

          1. Operate on row to make (row, col) == 1
          2. Make sure that all other values with the same variables (eg. all
             other coefficients in the same column) are zeroed out.
        """

        # Divide pivot row by pivot to make pivot == 1
        nrows, ncols = self.Tableau.shape
        self.Values[row, 0] /= self.Tableau[row, col]
        self.Tableau[row, :] /= self.Tableau[row, col]

        # Make
        for i in [ r for r in range(0, nrows) if r != row ]:
            coefficient = 0 - self.Tableau[i, col]
            reduce_row = coefficient * self.Tableau[row, :]
            self.Tableau[i, :] += reduce_row
            self.Values[i, 0] += coefficient * self.Values[row, 0]

    def interpret( self ):
        nrows, ncols = self.Tableau.shape
        solution = np.zeros(ncols)
        
        for i in range(0, ncols):
            var_col = self.Tableau[1:, i] # Skip objective row
            is_basic = np.count_nonzero(var_col) == 1
            if is_basic:
                idx = np.where(var_col > 0)
                solution[i] = self.Values[idx] / var_col[idx]

        return solution
            
    def solve( self ):
        """
        Solves the constructed LP problem.

        Returns:
          - OPTIMAL:     If a solution could be found
          - INFEASIBLE:  If a solution is impossible
        """

        while not self.isSolved():
            p_row, p_col = self.getPivot()

            if p_row == None or p_col == None:
                return ([], "INFEASIBLE")

            self.pivot( p_row, p_col )


        solution = self.interpret()
        return (solution[0:self.NumVars], "OPTIMAL")

    def __str__( self ):
        np.set_printoptions( precision=2, formatter={'all':lambda x: str(x) + '\t'} )
        return "Full Tableau:\n{0}".format( np.hstack([self.Tableau, self.Values]) )

if __name__ == "__main__":
    lp = LinearProgram( [40, 30] )
    lp.addConstraint( [1, 2], 16 )
    lp.addConstraint( [1, 1], 9  )
    lp.addConstraint( [3, 2], 24 )

    print(lp)
    print("")
    print(lp.solve())
    print(lp)
